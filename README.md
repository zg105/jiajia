# Individual Project1
jiajia
In this project, I create a static site with Zola, a Rust static site generator, that will hold all of the portfolio work in this class. After that, I set uo Gitlab CICD workflow to autinatically integrate and deploy my code. The webpage is hosted by Gitlab page at https://ids721-individual1-zg105-d9bc9938833a9dc4839ca4ba65f1396008e52f.pages.oit.duke.edu/


## Get Start
1. zola static webpage: I reused the zola static webpage in mini project1
running `zola serve` to validate the webpage works well at local machine:
![](pic/1.png)

2. set up gitlab workflow: Enabled the gitlab CICD workflow by setting the `.gitlab-ci.yml`
![](pic/2.png)

3. After setting the cicd workflow, gitlab pages is enabled automatically to host the static webpage:
![](pic/3.png)

## video link:
demo video: https://youtu.be/kJ8rSpfYy5A
