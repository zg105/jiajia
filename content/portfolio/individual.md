+++
title = "Individual Project1"
description = "Use Zola to develop a static site."
date = 2024-03-10T09:19:42+00:00
updated = 2024-03-10T09:19:42+00:00
draft = false
template = "portfolio/page.html"

[extra]
lead = "This is the readme of the <b>Individual Prject</b>."
+++

In this project, I create a static site with Zola, a Rust static site generator, that will hold all of the portfolio work in this class. Then I set up gitlab CICD workflow and deploy the static page using gitlab pages.

visit the page via https://ids721-individual1-zg105-d9bc9938833a9dc4839ca4ba65f1396008e52f.pages.oit.duke.edu/